#!/bin/sh

# Install dependencies
sudo apt-get install -y qemu-user-static


#update git repo
git submodule init
git submodule update

#create python environment
mkdir -p ./pyenv
python -m venv ./pyenv

#activate python environment
. ./pyenv/bin/activate

#install ros_cross_compile
pip3 install ros_cross_compile
