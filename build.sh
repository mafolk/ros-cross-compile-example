
. ./pyenv/bin/activate

ros_cross_compile . --custom-setup-script ./scripts/setup.sh --arch aarch64 --os ubuntu --rosdistro galactic  --colcon-defaults ./defaults.yaml
